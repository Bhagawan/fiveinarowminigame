package com.example.fiveinrowminigame.util

import androidx.annotation.Keep

@Keep
data class FiveInRowSplashResponse(val url : String)