package com.example.fiveinrowminigame.util

data class Coord(val x: Int, val y: Int)
