package com.example.fiveinrowminigame

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fiveinrowminigame.util.FiveInRowServerClient
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class FiveInRowSplashViewModel:ViewModel() {
    private var request: Job? = null
    private var requestInProcess = false

    private val _logoVisibility = MutableSharedFlow<Boolean>(1)
    val logoVisibility: SharedFlow<Boolean> = _logoVisibility

    private val _switchToMain = MutableSharedFlow<Boolean>(1)
    val switchToMain: SharedFlow<Boolean> = _switchToMain

    private val _showUrl = MutableSharedFlow<String>()
    val showUrl: SharedFlow<String> = _showUrl

    fun init(service: String, id: String) {
        requestInProcess = true
        showLogo()
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()).time)
                .replace("GMT", "")
            val splash = FiveInRowServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (requestInProcess) {
                        requestInProcess = false
                        hideLogo()
                    }
                    if (splash.body() != null) {
                        when(splash.body()!!.url) {
                            "no" ->  viewModelScope.launch { _switchToMain.emit(true) }
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                viewModelScope.launch { _switchToMain.emit(true) }
                            }
                            else -> viewModelScope.launch { _showUrl.emit("https://${splash.body()!!.url}") }
                        }
                    } else viewModelScope.launch { _switchToMain.emit(true) }
                } else viewModelScope.launch { _switchToMain.emit(true) }
            }
        } catch (e: Exception) {
            viewModelScope.launch { _switchToMain.emit(true) }
        }

    }

    private fun showLogo() {
        requestInProcess = true
        viewModelScope.launch { _logoVisibility.emit(true) }
    }

    private fun hideLogo() {
        viewModelScope.launch { _logoVisibility.emit(false) }
    }
}