package com.example.fiveinrowminigame.game

import com.example.fiveinrowminigame.util.Coord

data class GameState(val bubbles: List<ArrayList<Byte>>
                , val winBubbles: ArrayList<Coord>
                , val state: Int
                , val activeX: Float
                , val activeY: Float
                , val activeIsBlue: Boolean
                , val activeFalling: Boolean
                , val winTimer: Int)
