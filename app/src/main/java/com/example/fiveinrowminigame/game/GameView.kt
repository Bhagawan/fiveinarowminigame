package com.example.fiveinrowminigame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.fiveinrowminigame.R
import com.example.fiveinrowminigame.util.Coord
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class GameView (context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var bubbleWidth = 0.0f
    private val plankWidth = 10.0f
    private var offset = 0.0f

    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()
    private val plank = BitmapFactory.decodeResource(context.resources, R.drawable.wooden_plank)
    private val bubble = BitmapFactory.decodeResource(context.resources, R.drawable.bubble)
    private var bubbles = emptyList<ArrayList<Byte>>()

    private var activeBubbleX = 0.0f
    private var activeBubbleY = 0.0f
    private var activeBubbleColorIsBlue = true
    private var activeBubbleFalling = false

    private var winBubbles = ArrayList<Coord>()
    private var showWinTimer = 0

    private val redFilter = LightingColorFilter(Color.RED, 1)
    private val blueFilter = LightingColorFilter(Color.BLUE, 1)
    private val yellowFilter = LightingColorFilter(Color.YELLOW, 1)

    private var state = GAME
    private val aI = false

    companion object {
        const val GAME = 0
        const val END_SCREEN = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            bubbleWidth = (mHeight - plankWidth) / 8.0f
            restart()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
            updateActiveBubble()
            drawBubbles(it)
            if(state == END_SCREEN) checkWinTimer()
            if(state == END_SCREEN && showWinTimer > 60) drawEndScreen(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                if(state == GAME && !activeBubbleFalling) activeBubbleX = event.x.coerceAtLeast(bubbleWidth / 2 + offset).coerceAtMost(mWidth - bubbleWidth / 2 - offset)
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if(state == GAME && !activeBubbleFalling) activeBubbleX = event.x.coerceAtLeast(bubbleWidth / 2 + offset).coerceAtMost(mWidth - bubbleWidth / 2 - offset)
                return true
            }
            MotionEvent.ACTION_UP -> {
                if(state == GAME) {
                    if(!activeBubbleFalling) {
                        activeBubbleX = event.x.coerceAtLeast(bubbleWidth / 2 + offset).coerceAtMost(mWidth - bubbleWidth / 2 - offset)
                        activeBubbleFalling = true
                    }
                } else restart()
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public


    fun getState() = GameState(bubbles, winBubbles, state, activeBubbleX, activeBubbleY, activeBubbleColorIsBlue, activeBubbleFalling, showWinTimer)

    fun setState(gameState: GameState) {
        bubbles = gameState.bubbles
        winBubbles.clear()
        winBubbles.addAll(gameState.winBubbles)
        state = gameState.state
        activeBubbleX = gameState.activeX
        activeBubbleY = gameState.activeY
        activeBubbleFalling = gameState.activeFalling
        activeBubbleColorIsBlue = gameState.activeIsBlue
        showWinTimer = gameState.winTimer
    }

    /// Private

    private fun drawField(c: Canvas) {
        val p = Paint()
        c.drawBitmap(plank, null, Rect(mWidth - offset.toInt(), 0, mWidth, mHeight), p)
        c.drawBitmap(plank, null, Rect(0, 0, offset.toInt(), mHeight), p)
        c.drawBitmap(plank, null, Rect(0, mHeight - plankWidth.toInt(), mWidth, mHeight), p)
        for(n in bubbles.indices) {
            if(n > 0) {
                val left = (offset + n * (bubbleWidth + plankWidth) - plankWidth).toInt()
                c.drawBitmap(plank, null, Rect(left, bubbleWidth.toInt(), left + plankWidth.toInt(), mHeight - plankWidth.toInt()), p)
            }
        }
    }

    private fun drawBubbles(c: Canvas) {
        val p = Paint()
        for(column in bubbles.indices) {
            for(row in bubbles[column].indices) {
                if(bubbles[column][row] != 0.toByte()) {
                    p.colorFilter = if(bubbles[column][row] == 1.toByte()) blueFilter
                    else redFilter
                    if(winBubbles.contains(Coord(column, row))) p.colorFilter = yellowFilter
                    val left = offset + column * (bubbleWidth + plankWidth)
                    val top = mHeight - plankWidth - (row + 1) * bubbleWidth
                    c.drawBitmap(bubble, null, Rect(left.toInt(), top.toInt(), (left + bubbleWidth).toInt(), (top + bubbleWidth).toInt()), p)
                }
            }
        }
        if(winBubbles.size == 0) {
            p.colorFilter = if(activeBubbleColorIsBlue) blueFilter
            else redFilter
            c.drawBitmap(bubble, null, Rect((activeBubbleX - bubbleWidth / 2).toInt(), (activeBubbleY - bubbleWidth / 2).toInt()
                , (activeBubbleX + bubbleWidth / 2).toInt(), (activeBubbleY + bubbleWidth / 2).toInt()), p)
        }
    }

    private fun drawEndScreen(c: Canvas) {
        val p = Paint()
        p.style = Paint.Style.FILL
        p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
        c.drawRoundRect(mWidth * 0.25f, mHeight * 0.2f, mWidth * 0.75f, mHeight * 0.8f, 20.0f, 20.0f,p)
        val header: String
        if(winBubbles.size == 0) {
            p.color = ResourcesCompat.getColor(context.resources, R.color.grey_light, null)
            header = "Ничья"
        }
        else if(bubbles[winBubbles.first().x][winBubbles.first().y] == 1.toByte()) {
            p.color = Color.BLUE
            header = "Победил синий"
        }
        else {
            p.color = Color.RED
            header = "Победил красный"
        }
        p.strokeWidth = 5.0f
        p.style = Paint.Style.STROKE
        c.drawRoundRect(mWidth * 0.25f, mHeight * 0.2f, mWidth * 0.75f, mHeight * 0.8f, 20.0f, 20.0f,p)
        p.style = Paint.Style.FILL

        p.textAlign = Paint.Align.CENTER
        p.textSize = 40.0f
        p.color = Color.WHITE
        c.drawText(header, mWidth / 2.0f, mHeight * 0.3f, p)

        val size = mHeight * 0.4f
        restart?.let {
            c.drawBitmap(it, null, Rect((mWidth / 2 - size / 2).toInt(), (mHeight * 0.6f - size / 2).toInt(), (mWidth / 2 + size / 2).toInt(), (mHeight * 0.6f + size / 2).toInt()), p)
        }
    }

    private fun updateActiveBubble() {
        val speed = mHeight / 20
        if(activeBubbleFalling) {
            var targetColumn = (activeBubbleX - offset).toInt() / (bubbleWidth + plankWidth).toInt()
            if(bubbles[targetColumn].last() != 0.toByte()) {
                val free = ArrayList<Int>()
                for(c in bubbles.indices) if(bubbles[c].last() == 0.toByte()) free.add(c)
                if(free.size == 0) state = END_SCREEN
                else {
                    var nearest = 99
                    for(f in free) if((targetColumn - f).absoluteValue < (targetColumn - nearest).absoluteValue) nearest = f
                    targetColumn = nearest
                }
            }

            var targetRow = 0
            for(n in bubbles[targetColumn].indices) {
                if(bubbles[targetColumn][n] != 0.toByte()) targetRow = n + 1
            }
            if(activeBubbleX != (offset + targetColumn * (bubbleWidth + plankWidth) + bubbleWidth / 2.0f))  activeBubbleX = (offset + targetColumn * (bubbleWidth + plankWidth) + bubbleWidth / 2.0f)
            if(activeBubbleY + speed < mHeight - plankWidth - bubbleWidth / 2.0f - (targetRow * bubbleWidth))  activeBubbleY += speed
            else {
                bubbles[targetColumn][targetRow] = if (activeBubbleColorIsBlue) 1.toByte() else (-1).toByte()
                val win = checkWinForNew(targetColumn, targetRow)
                val draw = checkDraw()
                if(!win && ! draw) {
                    activeBubbleColorIsBlue = !activeBubbleColorIsBlue
                    activeBubbleY = bubbleWidth / 2.0f
                    if(aI) {
                        val free = ArrayList<Int>()
                        for(c in bubbles.indices) if(bubbles[c].last() == 0.toByte()) free.add(c)
                        activeBubbleX = offset + bubbleWidth / 2.0f + free.random() * bubbleWidth
                    } else {
                        activeBubbleX = mWidth / 2.0f
                        activeBubbleFalling = false
                    }
                } else {
                    activeBubbleFalling = false
                    state = END_SCREEN
                }
            }
        }
    }

    private fun checkWinForNew(column: Int, row: Int) : Boolean {
        val hor = ArrayList<Coord>() // -
        val ver = ArrayList<Coord>() // |
        val lTr = ArrayList<Coord>() // \
        val rTl = ArrayList<Coord>() // /

        for(d in 1..(bubbles.size - 1).coerceAtLeast(7)) {
            if(column - d in bubbles.indices) {
                if(bubbles[column - d][row] == bubbles[column][row]) hor.add(Coord(column - d, row))
                if(row - d in 0..6) {
                    if(bubbles[column - d][row - d] == bubbles[column][row]) rTl.add(Coord(column - d, row - d))
                }
                if(row + d in 0..6) {
                    if(bubbles[column - d][row + d] == bubbles[column][row]) lTr.add(Coord(column - d, row + d))
                }
            }
            if(column + d in bubbles.indices) {
                if(bubbles[column + d][row] == bubbles[column][row]) hor.add(Coord(column + d, row))
                if(row - d in 0..6) {
                    if(bubbles[column + d][row - d] == bubbles[column][row]) lTr.add(Coord(column + d, row - d))
                }
                if(row + d in 0..6) {
                    if(bubbles[column + d][row + d] == bubbles[column][row]) rTl.add(Coord(column + d, row + d))
                }
            }
            if(row - d in 0..6) {
                if(bubbles[column][row - d] == bubbles[column][row]) ver.add(Coord(column, row - d))
            }
            if(row + d in 0..6) {
                if(bubbles[column][row + d] == bubbles[column][row]) ver.add(Coord(column, row + d))
            }
        }
        winBubbles.clear()
        if(hor.size >= 4) winBubbles.addAll(hor)
        if(ver.size >= 4) winBubbles.addAll(ver)
        if(lTr.size >= 4) winBubbles.addAll(lTr)
        if(rTl.size >= 4) winBubbles.addAll(rTl)
        if(winBubbles.size >= 4 ) winBubbles.add(Coord(column, row))
        return winBubbles.size >= 5
    }

    private fun checkDraw() : Boolean {
        for(c in bubbles.indices) if(bubbles[c].last() == 0.toByte()) return false
        return true
    }

    private fun checkWinTimer() {
        if(state == END_SCREEN && showWinTimer <= 60) {
            if(winBubbles.size == 0) showWinTimer = 60
            else showWinTimer++
        }
    }

    private fun createField() {
        val columns : Int = (mWidth / (bubbleWidth + plankWidth)).toInt()
        offset = (mWidth % (bubbleWidth + plankWidth)) / 2.0f
        bubbles = List(columns) { ArrayList(List(7) { 0 }) }
    }

    private fun restart() {
        state = GAME
        createField()
        winBubbles.clear()
        showWinTimer = 0
        activeBubbleY = bubbleWidth / 2
        activeBubbleX = mWidth / 2.0f
        activeBubbleFalling = false
        activeBubbleColorIsBlue = Random.nextInt(2) == 0
    }


}