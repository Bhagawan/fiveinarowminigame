package com.example.fiveinrowminigame.game

import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {
    private var gameState: GameState? = null

    fun saveGameState(state: GameState) {
        gameState = state
    }

    fun getGameState() : GameState? = gameState
}